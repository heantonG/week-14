class Node:
    def __init__(self, data=None, next=None, prev=None):
        self.data = data
        self.next = next
        self.prev = prev

class DoubleLinkedList:
    def __init__(self):
        self.head = None

    def get_length(self):
        count = 0
        itr = self.head
        while itr:
            count+=1
            itr = itr.next

        return count
    
    def print_forward(self):
        if self.head is None:
            print("Linked list is empty")
            return
        itr = self.head
        # print(itr.prev.data)
       #很奇怪...这里的 itr.data 竟然不是 "big big shit".....
        llstr = ''
        while itr:
            llstr += str(itr.data)+' --> ' if itr.next else str(itr.data)
            itr = itr.next
        print(llstr)
    
    def print_backward(self):
        if self.head is None:
            print("Linked list is empty")
            return

        itr = self.head
        cou = 0

        while cou < self.get_length() - 1:
            itr = itr.next
            # print(itr.data)
            cou += 1
        llstr = ""
        while itr:
            llstr += str(itr.data)+' --> ' if itr.prev else str(itr.data)
            itr = itr.prev
        print(llstr)        
        

    def insert_at_end(self, data):
        if self.head is None:
            self.head = Node(data, None, None)
            return

        itr = self.head

        while itr.next:
#             print(itr.next.data)

            itr = itr.next
        itr.next = Node(data, None, itr)


    def insert_at_begin(self,data):
        if self.head is None:
            self.head = Node(data, None, None)
            return

        itr = self.head

        while itr.prev:
            itr = itr.prev
        itr.prev = Node(data, itr, None)

    def insert_values(self, data_list):
        self.head = None
        for data in data_list:
            self.insert_at_end(data)
    
    


# -------------------------------------------main------------------------------------------

if __name__ == '__main__':
    dll = DoubleLinkedList()
    dll.insert_values(["banana", "mango", "grapes", "orange", "apple", "pineapple"])
    dll. insert_at_begin("big big shit")
    #关于双向列表怎么取头还尚存疑问，但是。。。
    dll.print_forward()
    dll.print_backward()


